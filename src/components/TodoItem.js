const TodoItem = ({ todo, setRefresh }) => {

    const updateTodo = () => {
        todo.done = !todo.done

        fetch("https://628c3fa83df57e983ecb9cf8.mockapi.io/todoApp/" + todo.id, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(todo)
        }).then(() => {
            setRefresh(true)
            setTimeout(() => {
                alert('todo updated')
            }, 500)
        })
    }

    const deleteTodo = () => {
        
        fetch("https://628c3fa83df57e983ecb9cf8.mockapi.io/todoApp/" + todo.id, {
            method: "DELETE",
        }).then(() => {
            setRefresh(true);
            setTimeout(() => {
                alert('todo deleted.')
            }, 500)
        });
    };


    return (
        <li className={`${todo.done ? "checked" : ""}`}>
            <div onClick={updateTodo}>{todo.title}</div>
            <span className="close" onClick={deleteTodo}>x</span>
        </li>
    );
};

export default TodoItem;